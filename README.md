Código baixado do repositório:
https://github.com/rocketseat-content/youtube-serverless-thumb-generator

Video de Referência:
https://www.youtube.com/watch?v=qvetoR6V5ic&ab_channel=Rocketseat

Para conseguir ver as thumbnails sendo geradas o projeto precisa estar logado com sua conta da Vercel,
e executar o projeto com: 
vercel dev

Obs: procurar 'Dicas:' pelo projeto, pois foram as anotações realizadas através das dicas do video.