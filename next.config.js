module.exports = {
  target: 'serverless',
  // Dicas: configuração do webpack, de maneira fácil
  webpack: function (config) {
    // Dicas: para todo arquivo md, usar o 'raw-loader'
    config.module.rules.push({test:  /\.md$/, use: 'raw-loader'})
    
    return config
  }
}