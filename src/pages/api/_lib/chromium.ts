// Dica: chromium é o motor do navegador chrome
// Dica: puppeteer irá tirar uma foto da página gerada
// puppeteer docs: https://pptr.dev/
import puppeteer, { Page } from 'puppeteer-core'
import { getOptions } from './chromeOptions'

let _page: Page | null

async function getPage(isDev: boolean): Promise<Page> {
  // Dica: verifica se já uma instância do chrome aberta
  // caso exista, usa ela mesma, evitando inicializações desnecessárias 
  if (_page) {
    return _page
  }

  // Dica: caso não exista, esse é o processo para criar uma nova
  const options = await getOptions(isDev)
  const browser = await puppeteer.launch(options)

  _page = await browser.newPage()

  return _page
}

export async function getScreenshot(
  html: string,
  isDev: boolean
): Promise<Buffer> {
  const page = await getPage(isDev)

  // Dica: cria a tela de visualização do página, nesse caso o tamanho é o ideal para uma thumbnail
  await page.setViewport({ width: 1200, height: 630 })
  // Dica: abaixo será passado o código HTML que será alvo do screenshot
  await page.setContent(html)

  // Dicas: outras formas de salvar páginas, sem precisar criá-las
  // await page.goto('https://drive.google.com/file/d/1HsBuATZF0p5u-PW1RmWp6zdBrCJD6p82/view', {
  //   waitUntil: 'networkidle0',
  // });

  // await page.waitForSelector('#drive-viewer-video-player-object-0', {
  //   visible: true,
  // });

  const file = await page.screenshot({ type: 'png' })

  return file
}
